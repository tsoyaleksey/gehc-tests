package com.gehc.tests.api;

import com.gehc.pojos.Entries;
import com.gehc.pojos.Entry;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.SoftAssertions;
import org.testng.annotations.Test;

import java.util.List;

import static com.gehc.configs.Rest.makeRequestAndGetResponse;
import static com.gehc.queryParams.EntriesParams.Keys.CATEGORY;
import static com.gehc.queryParams.EntriesParams.Values.AUTHENTICATION_AND_AUTHORIZATION;
import static io.restassured.http.Method.GET;
import static org.apache.http.HttpStatus.SC_OK;

@Slf4j
public class PublicApiTest {
    private static final String ENTRIES_PATH = "/entries";

    @Test
    public void test() {
        Entries allEntries = makeRequestAndGetResponse(GET, spec -> spec.basePath(ENTRIES_PATH), SC_OK, Entries.class);

        Entries authenticationAndAuthorizationEntries = makeRequestAndGetResponse(GET, spec ->
                spec.basePath(ENTRIES_PATH).params(CATEGORY, AUTHENTICATION_AND_AUTHORIZATION), SC_OK, Entries.class);
        List<Entry> entryList = authenticationAndAuthorizationEntries.getEntries();

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(authenticationAndAuthorizationEntries.getCount())
                .describedAs("The count is equal to number of entries")
                .isEqualTo(entryList.size());
        softly.assertThat(allEntries.getEntries())
                .describedAs("All entries contains 'authentication & authorization' entries")
                .containsAll(entryList);
        softly.assertThat(allEntries.getEntries())
                .describedAs("Filtered entries equals to expected")
                .filteredOn(entry -> entry.getCategory().equals(AUTHENTICATION_AND_AUTHORIZATION))
                .isEqualTo(entryList)
                .hasSameSizeAs(entryList);
        softly.assertAll();

        log.info("'Authentication & Authorization' category has next entries: {}", entryList);
    }
}
