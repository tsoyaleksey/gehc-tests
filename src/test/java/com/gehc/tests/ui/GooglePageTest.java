package com.gehc.tests.ui;

import com.gehc.configs.Settings;
import com.gehc.pages.GoogleSearchPage;
import com.gehc.pages.GoogleSearchResultsPage;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;

public class GooglePageTest {
    private static final String SEARCH_TEXT = "Java";

    @BeforeMethod
    public void setUp() {
        open(Settings.getDefaultUiUrl());
    }


    @Test
    public void searchForTextTest() {
        new GoogleSearchPage().searchFor(SEARCH_TEXT);

        new GoogleSearchResultsPage()
                .printResultsLinks()
                .verifyFirstElementHasExpectedText(SEARCH_TEXT)
                .verifyLastElementHasNotExpectedText("Interview");
    }
}
