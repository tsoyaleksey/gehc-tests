package com.gehc.configs;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.http.Method;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

import java.util.function.Function;

import static com.gehc.configs.Settings.getDefaultApiUrl;
import static io.restassured.RestAssured.given;

public class Rest {
    private static final RequestSpecification SPECIFICATION = new RequestSpecBuilder()
            .setBaseUri(getDefaultApiUrl())
            .setContentType(ContentType.JSON)
            .build()
            .filters(new RequestLoggingFilter(LogDetail.ALL), new ResponseLoggingFilter(LogDetail.ALL));

    public static ValidatableResponse makeRequestAndGetResponse(Method method,
                                                                Function<RequestSpecification, RequestSpecification> function,
                                                                int statusCode) {
        return function.apply(given(SPECIFICATION)).request(method).then().statusCode(statusCode);
    }

    public static <T> T makeRequestAndGetResponse(Method method,
                                                  Function<RequestSpecification, RequestSpecification> function,
                                                  int statusCode,
                                                  Class<T> clazz) {
        return makeRequestAndGetResponse(method, function, statusCode).extract().as(clazz);
    }
}
