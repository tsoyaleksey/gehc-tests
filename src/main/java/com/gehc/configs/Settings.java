package com.gehc.configs;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.logevents.SelenideLogger;
import com.gehc.utils.PropertyReader;
import io.qameta.allure.selenide.AllureSelenide;

public final class Settings {
    private static final PropertyReader propertyReader = PropertyReader.getProperty("settings.properties");

    static {
        SelenideLogger.addListener("allure", new AllureSelenide());
        Configuration.browserSize = propertyReader.getValue("default.browserSize");
        Configuration.headless = true;
    }

    private Settings() {}

    public static String getDefaultUiUrl() {
        return propertyReader.getValue("default.ui.url");
    }

    public static String getDefaultApiUrl() {
        return propertyReader.getValue("default.api.url");
    }
}
