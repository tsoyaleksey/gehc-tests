package com.gehc.pojos;

import lombok.Data;
import java.util.List;

@Data
public class Entries {
    private int count;
    private List<Entry> entries;
}
