package com.gehc.utils;

import java.io.IOException;
import java.util.Properties;

public class PropertyReader {

    private static PropertyReader instance;
    private final Properties properties;

    private PropertyReader(String name) {
        properties = new Properties();
        try {
            properties.load(getClass().getClassLoader().getResourceAsStream(name));
        } catch (IOException e) {
            throw new RuntimeException("Can't load properties", e);
        }
    }

    public static PropertyReader getProperty(String name) {
        if (instance == null) {
            instance = new PropertyReader(name);
        }
        return instance;
    }

    public String getValue(String key) {
        return properties.getProperty(key);
    }
}
