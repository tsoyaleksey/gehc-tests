package com.gehc.pages;

import com.codeborne.selenide.ElementsCollection;
import lombok.extern.slf4j.Slf4j;

import static com.codeborne.selenide.Condition.partialText;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$$x;
import static org.apache.commons.lang3.StringUtils.substringBefore;
import static org.openqa.selenium.By.tagName;

@Slf4j
public class GoogleSearchResultsPage {
    private final ElementsCollection results = $$x("//div[@class='MjjYud']//div[@class='yuRUbf' and not (ancestor::ul)]").filter(visible);

    public ElementsCollection getResults() {
        return results;
    }

    public GoogleSearchResultsPage printResultsLinks() {
        getResults().asFixedIterable()
                .iterator().forEachRemaining(el ->
                        log.info("element link: {}", substringBefore(el.find(tagName("cite")).getText(), " ")));
        return this;
    }

    public GoogleSearchResultsPage verifyFirstElementHasExpectedText(String text) {
        getResults().first().shouldHave(partialText(text));
        return this;
    }

    public GoogleSearchResultsPage verifyLastElementHasNotExpectedText(String text) {
        getResults().last().shouldNotHave(partialText(text));
        return this;
    }
}
