package com.gehc.queryParams;

public interface EntriesParams {

    interface Keys {
        String CATEGORY = "Category";
    }

    interface Values {
        String AUTHENTICATION_AND_AUTHORIZATION = "Authentication & Authorization";
    }
}
