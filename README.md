# Getting started

Hello observer :)
I hope you enjoy my completed task!

## How to run tests?
- Please use jdk8 or above
- Use Maven cmd or your IDE for running TestNG tests
- I have added Allure report for demonstration purposes

```
mvn clean test io.qameta.allure:allure-maven:report
```

## Gitlab CI/CD 
- Please bear in mind I'm not an expert in Gitlab CI, it was a long time ago when I lastly used the Gitlab CI. I have tried to create a small pipeline that shows my passion for automation testing and that I could do anything for having successful results and happy customers, and people on the project. 
- you could check details in CI/CD -> Piplines tab. Btw, you could also check Allure report for the latest run. You need to go to CI/CD -> Jobs -> latest green result, and check 'Job artifacts' (Browse -> target/site/allure-maven-plugin/index.html).
- I used headless Chrome due to the Docker image limitations of this pipeline. Feel free to turn it off in Settings class config.

### Dependencies that I used for this task
- jdk8, maven, testNG, allure2, selenide, rest-assured, assertj, slf4j, jackson, lombok
- details you could find in pom.xml

<br>

## Thanks and have a nice day!
